import 'dart:math';

import 'package:TheClinic/Edit_Profile.dart';
import 'package:TheClinic/StartPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Dashboard_sidemenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Dashboard_sidemenu();
  }
}

class _Dashboard_sidemenu extends State<Dashboard_sidemenu> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff42CDA6),
      appBar: AppBar(
        leading: Builder(builder: (BuildContext context) {
          return new GestureDetector(
            onTap: () {
              Scaffold.of(context).openDrawer();
            },
            child: new Container(
              child: new Icon(Icons.menu),
              padding: new EdgeInsets.all(7.0),
            ),
          );
        }),
        title: Text(
          "Dashboard",
          style: TextStyle(color: Colors.black87),
        ),
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.only(left: 50, right: 50),
              child: new DrawerHeader(
                child: CircleAvatar(
                  minRadius: 10,
                  maxRadius: 15,
                  backgroundImage: AssetImage("assets/images/pic.png"),
                ),
              ),
            ),
            ListTile(
              leading: Text(
                "Login",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => StartPage()));
              },
            ),
            ListTile(
              leading: Text(
                "Edit Profile",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => Edit_profile()));
              },
            ),
            ListTile(
              leading: Text(
                "About Us",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              trailing: Icon(Icons.arrow_forward),
            ),
            ListTile(
              leading: Text(
                "Update",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              trailing: Icon(Icons.arrow_forward),
            ),
            ListTile(
              leading: Text(
                "Exit",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              trailing: Icon(Icons.arrow_forward),
            ),
          ],
        ),
      ),
      body: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: 180,
                width: 180,
                child: Card(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 20,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(2),
                        child: Image.asset(
                          "assets/images/appointment.png",
                          alignment: Alignment.center,
                        ),
                      ),
                      Text(
                        "Appointment Left",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                height: 180,
                width: 180,
                child: Card(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 20,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(2),
                        child: Image.asset(
                          "assets/images/meet.png",
                          alignment: Alignment.center,
                        ),
                      ),
                      Text(
                        "Meet",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Container(
                height: 180,
                width: 180,
                child: Card(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 20,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(2),
                        child: Image.asset(
                          "assets/images/supplies.png",
                          alignment: Alignment.center,
                        ),
                      ),
                      Text(
                        "Supplies",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                height: 180,
                width: 180,
                child: Card(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 20,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(2),
                        child: Image.asset(
                          "assets/images/prescription.png",
                          alignment: Alignment.center,
                        ),
                      ),
                      Text(
                        "Prescription",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
