import 'package:TheClinic/Dashboard_sidemenu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: camel_case_types
class Edit_profile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Edit_Profile();
  }
}

class _Edit_Profile extends State<Edit_profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 40.0),
              child: Container(
                child: Text(
                  "Edit Profile",
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      color: Colors.black87),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: Container(
                width: 190.0,
                height: 190.0,
                decoration: new BoxDecoration(
                    color: Colors.red,
                    shape: BoxShape.circle,
                    border: Border.all(style: BorderStyle.solid, width: 2),
                    image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: new AssetImage("assets/images/logo.jpg"))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 40, right: 40),
              child: Container(
                child: Column(
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(
                        hoverColor: Colors.red,
                        contentPadding: EdgeInsets.only(top: 0, left: 20),
                        hintText: 'First Name',
                        hintStyle: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        hoverColor: Colors.red,
                        contentPadding: EdgeInsets.only(top: 0, left: 20),
                        hintText: 'Last Name',
                        hintStyle: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        hoverColor: Colors.red,
                        contentPadding: EdgeInsets.only(top: 0, left: 20),
                        hintText: 'BirthDay',
                        hintStyle: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        hoverColor: Colors.red,
                        contentPadding: EdgeInsets.only(top: 0, left: 20),
                        hintText: 'Phone No.',
                        hintStyle: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    Container(
                      width: 400,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          color: Colors.cyan,
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    Dashboard_sidemenu()));
                          },
                          child: Text(
                            "Save",
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                          shape: new RoundedRectangleBorder(
                            side: BorderSide(color: Colors.cyan),
                            borderRadius: new BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      backgroundColor: Color(0xff42CDA6),
    );
  }
}
