import 'package:TheClinic/StartPage.dart';
import 'package:flutter/material.dart';

// ignore: camel_case_types
class Signup_Page extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Signup_Page();
  }
}

// ignore: camel_case_types
class _Signup_Page extends State<Signup_Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            child: Image.asset(
              "assets/images/logo.jpg",
            ),
          ),
          Container(
            child: Form(
                child: Padding(
              padding: const EdgeInsets.only(left: 40.0, right: 40, top: 8),
              child: Column(
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(
                      hoverColor: Colors.red,
                      contentPadding: EdgeInsets.only(top: 0, left: 20),
                      hintText: 'Username',
                      hintStyle: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                      hoverColor: Colors.red,
                      contentPadding: EdgeInsets.only(top: 0, left: 20),
                      hintText: 'Email Id',
                      hintStyle: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                      hoverColor: Colors.red,
                      contentPadding: EdgeInsets.only(top: 0, left: 20),
                      hintText: 'Password',
                      hintStyle: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Container(
                    width: 400,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        color: Color(0xff44CDA6),
                        onPressed: () {},
                        child: Text("SignUP"),
                        shape: new RoundedRectangleBorder(
                          side: BorderSide(color: Color(0xff44CDA6)),
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context,
                          MaterialPageRoute(builder: (context) => StartPage()));
                    },
                    child: Text(
                      "SignIN",
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                  )
                ],
              ),
            )),
          )
        ],
      ),
    );
  }
}
