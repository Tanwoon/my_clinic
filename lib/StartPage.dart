import 'package:TheClinic/Edit_Profile.dart';
import 'package:TheClinic/Signup_Page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StartPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StartPage();
  }
}

class _StartPage extends State<StartPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              child: Image.asset("assets/images/logo.jpg"),
            ),
            Form(
              child: Column(
                children: <Widget>[
                  //Text("rohit"),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 40, right: 40, top: 20),
                    child: Container(
                      height: 40,
                      width: 400,
                      child: TextFormField(
                        decoration: InputDecoration(
                          hoverColor: Colors.red,
                          contentPadding: EdgeInsets.only(top: 0, left: 20),
                          hintText: 'Username',
                          hintStyle: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 40.0, right: 40),
                    child: Container(
                      height: 40,
                      width: 400,
                      child: TextFormField(
                        decoration: InputDecoration(
                          hoverColor: Colors.red,
                          contentPadding: EdgeInsets.only(top: 0, left: 20),
                          hintText: 'Password',
                          hintStyle: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 40, right: 40),
                    child: Container(
                      width: 400,
                      child: RaisedButton(
                        color: Color(0xff44CDA6),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  Edit_profile()));
                        },
                        child: Text("Login"),
                        shape: new RoundedRectangleBorder(
                          side: BorderSide(color: Color(0xff44CDA6)),
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      "Forget Password",
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      /*  bottomNavigationBar: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          BottomAppBar(
            child: Text(
              "Don't have an account?",
              style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w900,
                  color: Colors.black),
            ),
            elevation: 0,
          )*/

      backgroundColor: Colors.white,
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        child: GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => Signup_Page()));
          },
          child: Text(
            "Don't have an account",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 12,
                color: Colors.black87,
                fontWeight: FontWeight.w900),
          ),
        ),
        elevation: 0,
      ),
    );
  }
}
